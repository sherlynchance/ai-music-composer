# AI Music Composer

This is our final project for Intelligent Systems.


Our solutions for creating music composer are:

*  Using Keras library to create LSTM model which is then trained and used to generate musical notation for our music output.
*  Notes and chords are used as our input and output of the LSTM network
*  Using Music21, a python toolkit that allows us to generate music examples and study music, which also allows us to create Note and Chords so that we can make our own MIDI files easily


Here, we are using 65 datasets of midi files for training.

First, run the train.py. This code is used for training the model

At every Epoch, a checkpoint is created so you can stop the execution anytime as long as you are satisfied with the results.

Next, run the predict.py. This code predicts the sequence of the notes and output them into a midi file

After inserting the filepath of the .hdf5 file that you trained earlier simply run the code.

You can run the code several times, and the code will produce a different output of notes everytime. 