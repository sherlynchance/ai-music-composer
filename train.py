import numpy as np
import glob
import pickle
from music21 import converter, instrument, note, chord
from keras.models import Sequential
from keras.layers import LSTM, Dropout, Dense, Activation, BatchNormalization
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint

def get_notes():

	notes = []

	# parse all midi files in the datasets directory
	for file in glob.glob("datasets/*.mid"):
		midi = converter.parse(file)
	
		print("Parsing " + file)

		notes_to_parse = None

		#execute if file has instrument parts
		try: 
			s2 = instrument.partitionByInstrument(midi)
			notes_to_parse = s2.parts[0].recurse()

		# throw exception if file has notes with flat structure
		except: 
			notes_to_parse = midi.flat.notes

		for element in notes_to_parse:
			if isinstance(element, note.Note):
				notes.append(str(element.pitch))
			elif isinstance(element, chord.Chord):
				notes.append('.'.join(str(n) for n in element.normalOrder))

		# stores all parsed notes into the direcotry "data/notes"
	with open('data/notes', 'wb') as filepath:
		pickle.dump(notes, filepath)
			
	return notes

def prep_sequences(notes, n_vocab):

	# model predicts the next note based on previous 100 notes
	seq_length = 100

	# get all pitch names
	pitchnames = sorted(set(item for item in notes))

    # create a dictionary to map pitches to integers
	note_to_int = dict((note, number) for number, note in enumerate(pitchnames))
	
	network_input = []
	network_output = []

    # create input sequences and the corresponding outputs
	for i in range(0, len(notes) - seq_length, 1):
		seq_in = notes[i:i + seq_length]
		seq_out = notes[i + seq_length]
		network_input.append([note_to_int[char] for char in seq_in])
		network_output.append(note_to_int[seq_out])

	n_patterns = len(network_input)

    # reshape the input into a format compatible with LSTM layers
	network_input = np.reshape(network_input, (n_patterns, seq_length, 1))
 
	network_input = network_input / float(n_vocab)
	network_output = np_utils.to_categorical(network_output)

	return (network_input, network_output)	

# defines the structure of the neural network (LSTM)
def create_network(network_input, n_vocab):

	model = Sequential()
	# input shape = (number of time steps, number of input units)
    # return_sequences=True, each time step, a new character is generated as output
	model.add(
	LSTM(
		512,
		recurrent_dropout=0.3,
		return_sequences=True,
		input_shape=(network_input.shape[1], network_input.shape[2])
		))
    # prevent overfitting by ignoring randomly selected neurons during training, reduces the sensitivity to the specific weights of individual neurons. 
	model.add(LSTM(512, return_sequences=True, recurrent_dropout=0.3))
	model.add(LSTM(512))
	model.add(BatchNormalization())
	model.add(Dropout(0.3))
	model.add(Dense(256))
	model.add(Activation('relu'))
	model.add(BatchNormalization())
	model.add(Dropout(0.3))
	model.add(Dense(n_vocab))
	model.add(Activation('softmax'))
	model.compile(loss="categorical_crossentropy", optimizer="rmsprop")

	return model

def train_model(model, network_input, network_output):

	filepath = "epochs/weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
	checkpoint = ModelCheckpoint(
		filepath,
		monitor='loss',
		verbose=0,
		save_best_only=True, # in case of overfitting
		mode='min'
	)

	callbacks_list = [checkpoint]

	model.fit(network_input, network_output, epochs=10, batch_size=128, callbacks=callbacks_list)

def main():

	notes = get_notes()
	n_vocab = len(set(notes))
	network_input, network_output = prep_sequences(notes, n_vocab)
	model = create_network(network_input, n_vocab)

	train_model(model, network_input, network_output)

main()